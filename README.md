CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Imagepin module enables users to pin any widget on content images.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/imagepin

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/imagepin


REQUIREMENTS
------------

This module required https://www.drupal.org/project/jquery_ui_droppable
to be installed.


RECOMMENDED MODULES
-------------------

When Slick is installed, the Imagepin module automatically displays your widgets
as a swipeable carousel.

 * https://drupal.org/project/slick


INSTALLATION
------------

 * Install the Imagepin module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > [Content type to
       edit] > Manage display > [Display mode to edit] and select the contextual
       link icon on the field you wish to edit.
    3. This module gives you the option for enabling users to pin widgets
       for the Image and Responsive image formatter. Enable it as you like.
    4. Choose a proper image style for dragging the pins around, and optionally
       set a breakpoint for mobile and desktop resolutions.
    5. Editors now get a link on image fields for pinning widgets on each
       uploaded image.
    6. Create your widgets, drag their pins on the image, and save the
       positions.
    7. View your content.


MAINTAINERS
-----------

 * Maximilian Haupt (mxh) - https://www.drupal.org/u/mxh

Supporting organization:

 * Hubert Burda Media - https://www.drupal.org/hubert-burda-media
