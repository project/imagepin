<?php

namespace Drupal\imagepin;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Defines class for render imagepin.
 */
class ImagepinRender implements TrustedCallbackInterface {

  /**
   * Defines the trusted callbacks function.
   */
  public static function trustedCallbacks() {
    return ['preRender', 'postRender'];
  }

  /**
   * Adds the imagepin widgets element on the given element for an image item.
   *
   * @see imagepin_attach()
   */
  public static function preRender($element) {
    if (!empty($element['#item']) && !empty($element['#imagepin'])) {
      $image_fid = $element['#item']->get('target_id')->getValue();
      if (!empty($element['#imagepin']['widgets'][$image_fid])) {
        $attach_id = Crypt::randomBytesBase64(2);
        $element['#item_attributes']['data-imagepin-attach-from'] = $attach_id;
        $element['#imagepin_widgets'] = [
          '#theme' => 'imagepin_widgets',
          '#widgets' => $element['#imagepin']['widgets'][$image_fid],
          '#attach_id' => $attach_id,
          '#breakpoint' => $element['#imagepin']['settings']['breakpoint'],
        ];
      }
      unset($element['#imagepin']);
    }
    return $element;
  }

  /**
   * Adds the markup for imagepin widgets on an element.
   *
   * @see imagepin_attach()
   */
  public static function postRender($markup, $element) {
    $widgets_html = \Drupal::service('renderer')->render($element['#imagepin_widgets']);
    return $markup . $widgets_html;
  }

}
